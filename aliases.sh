#!/usr/bin/env bash
export DJANGO_SETTINGS_MODULE=djotali.settings.development
alias worker="celery -A djotali worker -l info"
alias seed="python manage.py seed"
alias reset_migrations="python manage.py migrate core campaigns contacts zero"
alias migrate="python manage.py migrate"
alias make_migration="python manage.py makemigrations"
alias run="python manage.py runserver"
alias shell="python manage.py shell"
alias flower="celery -A djotali flower"