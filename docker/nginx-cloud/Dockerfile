FROM python:3.6 as build

WORKDIR /var/data

RUN apt-get update && apt-get -y install gettext

COPY requirements.txt .

RUN pip install -q -r requirements.txt

COPY . .

RUN python manage.py collectstatic --noinput && python manage.py compress --force && \
    django-admin compilemessages -l fr_fr

FROM nginx:1.13-alpine

RUN echo http://dl-2.alpinelinux.org/alpine/edge/community/ >> /etc/apk/repositories
RUN apk --no-cache add shadow
RUN groupadd -r www-data && useradd -r -g www-data www-data
RUN usermod -a -G root www-data && chown -R www-data /var/cache/nginx && \
rm -rf /etc/nginx/conf.d/default.conf && touch /var/run/nginx.pid && chown www-data /var/run/nginx.pid

ADD docker/nginx-cloud/sites-enabled/ /etc/nginx/sites-enabled

ADD docker/nginx-cloud/nginx.conf /etc/nginx/nginx.conf

RUN chown -R www-data /etc/nginx

COPY --from=build /var/data/static /www/djotali/static

USER www-data

CMD envsubst '$SERVER_NAME' < /etc/nginx/sites-enabled/djotali.conf.tplt > /etc/nginx/sites-enabled/djotali.conf && nginx -g 'daemon off;'
