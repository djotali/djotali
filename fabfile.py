import os

from fabric.context_managers import settings, hide, shell_env
from fabric.operations import local, put, run
from versio.version import Version
from versio.version_scheme import Pep440VersionScheme

MAJOR = 'major'
MINOR = 'minor'
RC = 'rc'
RELEASE = 'release'

_local_docker_compose_tplt_filename = 'docker-compose.local.tplt.yml'
_cloud_docker_compose_tplt_filename = 'docker-compose.cloud.tplt.yml'
_default_secret_key = 'vumrpne5wvp499q632@#s+ik*0vh^(&il!+9&0m0s7c*mhcd1^'
_default_postgres_user = 'admin'
_default_postgres_pwd = 'changeme'
_default_redis_pwd = 'changeme'
_default_admin_pwd = 'changeme'

_version_filename = 'VERSION'
with open(_version_filename) as _version_file:
    _version = _version_file.readlines()[0].strip()


def _get_result(cmd='echo "Hello, World!'):
    with hide('output', 'running', 'warnings'), settings(warn_only=True):
        result = local(cmd, capture=True)
        return result if result else ''


def bump_and_write_version(bump_field=RC):
    assert bump_field in [RC, MINOR, MAJOR]
    local('git fetch --tags -q')
    versions = _get_result('git tag'.format(_version)).split('\n')

    parsed_version = Version(_get_version(), scheme=Pep440VersionScheme)
    _bump_version(parsed_version, bump_field)
    while str(parsed_version) in versions:
        _bump_version(parsed_version, bump_field)
    _write_version(str(parsed_version))


def _write_version(version):
    with open(_version_filename, 'w') as version_file:
        version_file.write(version)


def _bump_version(parsed_version, bump_field):
    if RC == bump_field and RC not in str(parsed_version):
        # First bump minor release
        parsed_version.bump(RELEASE, 1)
        # 4 times to get directly to rc
        # 1st for alpha, 2nd for beta, 3rd for c (?) and 4th for rc1
        parsed_version.bump('pre', 0)
        parsed_version.bump('pre', 0)
        parsed_version.bump('pre', 0)
        parsed_version.bump('pre', 0)
    elif RC == bump_field and RC in str(parsed_version):
        parsed_version.bump('pre', 1)
    elif bump_field == MINOR:
        parsed_version.bump(RELEASE, 1)
    elif bump_field == MAJOR:
        parsed_version.bump(RELEASE, 0)


def tag_version(bump_field=RC, version=None):
    """
    Tag and push version
    :param bump_field: dev || minor || major
    :param version: force version to write instead of bumping existing version. Is currently useful when you have to
    create first RC of a major version.
    Be careful not setting an existing version
    """
    if version:
        _write_version(version)
    else:
        bump_and_write_version(bump_field)

    version = _get_version()
    local(
        'git commit -a -m "Version {0}" && git tag -a {0} -m "Version {0}" && '
        'git push -q && '
        'git push --tags -q'.format(version)
    )


def _get_version():
    with open(_version_filename) as version_file:
        return version_file.readlines()[0].strip()


def build_djotali_img(version=_get_version()):
    djotali_img_name = 'ekougs/djotali:{}'.format(version)
    local('docker build -t {} --file docker/djotali/Dockerfile .'.format(djotali_img_name))


def push_djotali_img(version=_get_version()):
    build_djotali_img(version)
    djotali_img_name = 'ekougs/djotali:{}'.format(version)
    local('docker push {}'.format(djotali_img_name))


def build_djotali_proxy_img(env, version=_get_version()):
    djotali_proxy_img_name = 'ekougs/djotali_proxy:{}'.format(version)
    # Collect static and i18n files
    local('docker build -t {} --file docker/nginx-{}/Dockerfile .'.format(djotali_proxy_img_name, env))


def push_djotali_proxy_img(version=_get_version()):
    build_djotali_proxy_img('cloud', version)
    djotali_proxy_img_name = 'ekougs/djotali_proxy:{}'.format(version)
    local('docker push {}'.format(djotali_proxy_img_name))


def build_images(version=_get_version(), env='local', postgres_data_dir='./data/postgresql',
                 redis_data_dir='./data/redis'):
    with shell_env(VERSION=version, PROXY_DOCKERFILE='docker/nginx-{}/Dockerfile'.format(env),
                   POSTGRES_DATA_DIR=postgres_data_dir, REDIS_DATA_DIR=redis_data_dir):
        local('docker-compose build')


def push_images(version=_get_version(), env='local', postgres_data_dir='./data/postgresql',
                redis_data_dir='./data/redis'):
    build_images(version, env, postgres_data_dir, redis_data_dir)
    with shell_env(VERSION=version, PROXY_DOCKERFILE='docker/nginx-{}/Dockerfile'.format(env),
                   POSTGRES_DATA_DIR=postgres_data_dir, REDIS_DATA_DIR=redis_data_dir):
        local('docker-compose push')


def launch_containers_local(postgres_user=_default_postgres_user, postgres_pwd=_default_postgres_pwd, redis_pwd=_default_redis_pwd,
                            secret_key='vumrpne5wvp499q632@#s+ik*0vh^(&il!+9&0m0s7c*mhcd1^', debug=True,
                            populate_db=True, admin_pwd=_default_admin_pwd, settings_env='staging',
                            email_pwd=None, messagebird_token=None, build=True):
    # Launch compose
    with shell_env(POSTGRES_USER=postgres_user, POSTGRES_PASSWORD=postgres_pwd, REDIS_PASSWORD=redis_pwd,
                   DATABASE_URL=_build_dj_db_uri(postgres_user, postgres_pwd), SECRET_KEY=secret_key, DEBUG=str(debug),
                   POPULATE_DB=str(populate_db), ADMIN_PWD=admin_pwd,
                   DJANGO_SETTINGS_MODULE=('djotali.settings.' + settings_env), EMAIL_PASSWORD=email_pwd,
                   MESSAGEBIRD_TOKEN=messagebird_token, VERSION=_get_version(),
                   PROXY_DOCKERFILE='./docker/nginx-local/Dockerfile', CELERY_BROKER_URL=_build_redis_uri(redis_pwd)):
        if str(build) == str(True):
            local('docker-compose build')
        local('docker-compose up')


def deploy(version, postgres_user, postgres_pwd, redis_pwd, secret_key, admin_pwd, debug=False, populate_db=True,
           settings_env='staging', email_pwd=None, messagebird_token=None, server_name='test-app.djotali.com'):
    # Put our docker-compose file on the server
    docker_compose_location = '/var/local/djotali'
    put('docker-compose-up.yml', docker_compose_location + '/docker-compose.yml')

    # Launch docker-compose on the server
    with shell_env(POSTGRES_USER=postgres_user, POSTGRES_PASSWORD=postgres_pwd, REDIS_PASSWORD=redis_pwd,
                   DATABASE_URL=_build_dj_db_uri(postgres_user, postgres_pwd), SECRET_KEY=secret_key, DEBUG=str(debug),
                   POPULATE_DB=str(populate_db), ADMIN_PWD=admin_pwd,
                   DJANGO_SETTINGS_MODULE=('djotali.settings.' + settings_env), EMAIL_PASSWORD=email_pwd,
                   MESSAGEBIRD_TOKEN=messagebird_token, SERVER_NAME=server_name, VERSION=version,
                   CELERY_BROKER_URL=_build_redis_uri(redis_pwd)):
        run('cd {} && docker-compose up -d --no-build'.format(docker_compose_location))


def _build_file_from_tplt(tplt_location, output_location, **kwargs):
    for key, value in kwargs.items():
        os.environ[key] = value
    with open(tplt_location, 'r') as templated_file, open(output_location, 'w') as output_file:
        for templated_line in templated_file:
            output_file.writelines(os.path.expandvars(templated_line))


def _build_dj_db_uri(postgres_user, postgres_pwd):
    return 'postgresql://{0}:{1}@db/{0}'.format(postgres_user, postgres_pwd)


def _build_redis_uri(redis_pwd):
    return 'redis://:{}@redis:6379/0'.format(redis_pwd)
