import glob
from distutils.core import setup

from pip.req import parse_requirements
from setuptools import find_packages

requirements = [str(ir.req) for ir in parse_requirements('requirements.txt', session='hack')]

data_files_extensions = ['html', 'ini', 'tplt', 'css', 'js', 'txt']
data_files = []
prefix_len = len('djotali/')
for extension in data_files_extensions:
    data_files += [data_file[prefix_len:] for data_file in glob.iglob('djotali/**/*.' + extension, recursive=True)]

setup(
    name='djotali',
    packages=find_packages(exclude=('venv',)),
    url='',
    license='MIT',
    author='Pascal Ekouaghe',
    author_email='ekougs@gmail.com',
    description='Our SAAS solution to easily send messages to your clients',
    install_requires=requirements,
    package_data={
        'djotali': data_files
    },
    scripts=['scripts/launch_djotali', 'manage.py'],
)
