(function () {
    if (!djotali || !djotali.linkedModels) {
        // TODO improve logging or js delivery files building
        console.error('Check your JS files imports order.');
        return;
    }
    djotali.linkedModels.initComponent(
        'id_contacts_container',
        {
            headers: [
                {
                    label: 'Nom',
                    dataId: 'last_name',
                },
                {
                    label: 'Prénom',
                    dataId: 'first_name',
                },
                {
                    label: 'Téléphone',
                    dataId: 'phone_number',
                },
            ],
            currentElementsApiUrl: "/api/contacts-groups/" + djotali.linkedModels.objectId + "/contacts",
            searchElementsApiUrl: "/api/contacts?filter=",
        }
    );
})();