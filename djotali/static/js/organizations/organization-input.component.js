(function () {
    Vue.component('organization-input', {
        data: function () {
            return {
                "organization": "",
                "exists": false,
            };
        },
        methods: {
            checkIfOrganizationExists: function () {
                var data = this.$data;
                axios.get("/api/organizations/?filter=" + data.organization)
                     .then(function (response) {
                         var organizations = response.data;
                         data.exists = organizations && organizations.length == 1;
                     });
            },
        },
        template: '<div class="input-group">' +
                  '  <input id="id_organization" name="organization" type="text" v-on:keydown="checkIfOrganizationExists()" v-model="organization"' +
                  '         class="form-control">' +
                  '  <span class="input-group-addon" v-if="!exists" style="border: none; background-color: transparent">' +
                  '     <i id="new_organization" class="fa fa-plus text-success"></i>' +
                  '  </span>' +
                  '</div>'
    });

    new Vue({
        el: '#signupform',
    });

})();
