(function () {
    if (!window['djotali']) {
        window['djotali'] = {};
    }
    var djotali = window['djotali'];
    djotali.linkedModels = djotali.linkedModels || {};

    djotali.linkedModels.initPaginator = function initPaginator() {
        Vue.component('pagination', {
            computed: {
                page: function () {
                    return this.$store.state.page;
                },
                total: function () {
                    return this.$store.getters.total;
                },
            },
            methods: {
                hasPrevious: function hasPrevious() {
                    return this.$store.state.page > 1;
                },
                hasNext: function hasNext() {
                    return this.$store.state.page < this.$store.getters.total;
                },
                previous: function previous() {
                    this.$store.dispatch('previous');
                },
                next: function next() {
                    this.$store.dispatch('next');
                },
            },
            template: '<ul class="pagination">' +
                      ' <li class="page-item" v-if="hasPrevious()">' +
                      '  <a class="page-link" tabindex="-1" v-on:click="previous()">' +
                      '      <i class="fa fa-angle-left"></i>' +
                      '      <span class="sr-only">Précédent</span>' +
                      '  </a>' +
                      ' </li>' +
                      ' <li class="page-item">' +
                      '  <span>' +
                      '      Page {{ page }} sur {{ total }}.' +
                      '  </span>' +
                      ' </li>' +
                      ' <li class="page-item" v-if="hasNext()">' +
                      '  <a class="page-link" v-on:click="next(); return false;">' +
                      '      <i class="fa fa-angle-right"></i>' +
                      '      <span class="sr-only">Suivant</span>' +
                      '  </a>' +
                      ' </li>' +
                      '</ul>',
        });
    }
})();