(function () {
    if (!window['djotali']) {
        window['djotali'] = {};
    }
    var djotali = window['djotali'];
    djotali.linkedModels = djotali.linkedModels || {};

    djotali.linkedModels.initSearchBar = function initSearchBar(structure) {
        Vue.component('linked-models-search-bar', {
            data: function data() {
                return {
                    reprTemplate: '{{ item.repr }}',
                    asyncUrl: structure.searchElementsApiUrl,
                };
            },
            methods: {
                addRelation: function addRelation(item) {
                    this.$store.dispatch('addRelation', item);
                }
            },
            template: '<djotali-typeahead v-on:confirmedElement="addRelation" :async-url="asyncUrl">' +
                      '</djotali-typeahead>'
        });
    }
})();