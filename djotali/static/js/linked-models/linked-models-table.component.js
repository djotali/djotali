(function () {
    if (!window['djotali']) {
        window['djotali'] = {};
    }
    var djotali = window['djotali'];
    djotali.linkedModels = djotali.linkedModels || {};

    djotali.linkedModels.initLinkedModelsTable = function initLinkedModelsTable(structure) {
        Vue.component('linked-models-table', {
            data: function () {
                return {
                    headers: structure.headers,
                }
            },
            computed: {
                models: function () {
                    return this.$store.getters.getModels;
                }
            },
            created: function () {
                this.$store.dispatch('initModels');
            },
            methods: {
                deleteRelation: function deleteRelation(model) {
                    this.$store.dispatch('deleteRelation', model);
                }
            },
            template: '<table class="table color-table info-table">' +
                      '  <thead class="thead-default">' +
                      '     <tr>' +
                      '         <th v-for="header in headers">' +
                      '             {{ header.label }}' +
                      '         </th>' +
                      '         <th>' +
                      '             Actions' +
                      '         </th>' +
                      '     </tr>' +
                      '  </thead>' +
                      '  <tbody v-if="models">' +
                      '     <tr v-for="model in models">' +
                      '         <td v-for="header in headers">' +
                      '             {{ model[header.dataId] }}' +
                      '         </td>' +
                      '         <td>' +
                      '             <a v-on:click="deleteRelation(model)" style="cursor: pointer"><i class="icon-trash text-danger"></i></a>' +
                      '         </td>' +
                      '     </tr>' +
                      '  </tbody>' +
                      '</table>'
        });
    }
})();