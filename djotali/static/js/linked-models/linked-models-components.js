(function () {
    if (!window['djotali']) {
        window['djotali'] = {};
    }
    var djotali = window['djotali'];
    djotali.linkedModels = djotali.linkedModels || {};
    djotali.linkedModels.initComponent = initLinkedModelsComponent;

    function initLinkedModelsComponent(containerId, structure) {
        if (!djotali.linkedModels.initPaginator || !djotali.linkedModels.initLinkedModelsTable || !djotali.linkedModels.initStore) {
            throw new Error('Check your scripts imports, some files are missing.');
        }

        djotali.linkedModels.initPaginator();
        djotali.linkedModels.initLinkedModelsTable(structure);
        djotali.linkedModels.initSearchBar(structure);

        var changesId = containerId + '_input';
        Vue.component('linked-models-input', {
            computed: {
                modelIds: function () {
                    var modelIds = this.$store.state.models.map(function (model) {
                        return model.id;
                    });
                    return JSON.stringify(modelIds);
                },
            },
            template: '<input id="' + changesId + '" name="' + changesId + '" type="hidden" v-model="modelIds">'
        });

        var store = djotali.linkedModels.initStore(structure);

        new Vue({
            el: '#' + containerId,
            store: store,
            components: {
                'djotali-typeahead': djotali.components.typeahead
            },
        });
    }
})();
