(function () {
    if (!window['djotali']) {
        window['djotali'] = {};
    }
    var djotali = window['djotali'];
    djotali.linkedModels = djotali.linkedModels || {};

    var itemsPerPage = 5;
    djotali.linkedModels.initStore = function initStore(structure) {
        return new Vuex.Store({
            state: {
                models: [],
                page: 1,
                total: 1,
            },
            mutations: {
                updatePage: function updatePageMeta(state, page) {
                    state.page = page;
                },
                updateModels: function updatePageMeta(state, models) {
                    state.models = models;
                },
                deleteRelation: function deleteRelation(state, model) {
                    var idx = state.models.indexOf(model);
                    state.models.splice(idx, 1);
                },
                addRelation: function deleteRelation(state, model) {
                    if (state.models.indexOf(model) != -1) {
                        return;
                    }
                    state.models.unshift(model);
                },
            },
            actions: {
                initModels: function initModels(context) {
                    axios.get(structure.currentElementsApiUrl)
                         .then(function (response) {
                             var models = response.data;
                             context.commit('updateModels', models);
                             context.commit('updatePage', 1);
                         });
                },
                next: function toPage(context) {
                    context.commit('updatePage', context.state.page + 1);
                },
                previous: function toPage(context) {
                    context.commit('updatePage', context.state.page - 1);
                },
                deleteRelation: function deleteRelation(context, model) {
                    context.commit('deleteRelation', model);
                },
                addRelation: function deleteRelation(context, model) {
                    context.commit('addRelation', model);
                },
            },
            getters: {
                getModels: function getModels(state) {
                    var startIdx = (state.page - 1) * itemsPerPage;
                    var endIndex = startIdx + itemsPerPage;
                    if (endIndex >= state.models.length) {
                        endIndex = state.models.length;
                    }
                    return state.models.slice(startIdx, endIndex)
                },
                total: function total(state) {
                    var total = Math.floor(state.models.length / itemsPerPage);
                    if (state.models.length % itemsPerPage !== 0) {
                        total++;
                    }
                    return total > 0 ? total : 1;
                },
            }
        });
    }
})();