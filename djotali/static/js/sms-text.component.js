(function () {

    Vue.component('sms-text', {
        props: ['message', 'sender'],
        data: function data() {
            var rawSms = this.$props.message;
            if (!rawSms) {
                rawSms = '\n' + this.$props.sender;
            }
            var smsMetaData = SmsCounter.count(rawSms);
            return {
                remainingCharacters: smsMetaData.remaining,
                rawSms: rawSms
            };
        },
        computed: {
            smsInputMaxLength: function smsInputMaxLength() {
                return 160 - (160 - this.$data.remainingCharacters - this.$data.rawSms.length);
            }
        },
        methods: {
            validateSms: function validateSms(event) {
                var rawSms = event.target.value;
                var smsMetaData = SmsCounter.count(rawSms);
                this.$data.remainingCharacters = smsMetaData.remaining;
                this.$data.rawSms = rawSms;
            }
        },
        template: '<div>' +
        '<textarea id="id_message" name="message" class="form-control" rows="3" v-on:keyup="validateSms($event)" v-bind:maxlength="smsInputMaxLength"' +
        '          placeholder="Veuillez entrer le contenu de votre sms contenant un maximum de 160 caractères">' +
        '{{ rawSms }}' +
        '</textarea>' +
        '<div class="float-right">{{ remainingCharacters }} caractères restant</div>' +
        '<div class="float-right text-warning">Nous vous conseillons fortement d\'ajouter votre nom/sender ID à la fin du sms pour éviter toute confusion pour votre client.</div>' +
        '</div>'

    });

    new Vue({
        el: '#message_container'
    });
})();