(function () {
    if (!window['djotali']) {
        window['djotali'] = {};
    }
    var djotali = window['djotali'];
    djotali.components = djotali.components || {};
    djotali.components.typeahead = Vue.component('djotali-typeahead', {
        props: ["asyncUrl"],
        data: function () {
            return {
                selectedModel: undefined,
            }
        },
        mounted: function initTypeAhead() {
            var vm = this;

            var modelSource = new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace,
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                identify: function (model) {
                    return model.id;
                },
                remote: {
                    url: vm.asyncUrl + '%QUERY',
                    wildcard: '%QUERY',
                    transform: function (response) {
                        return response.results;
                    }
                }
            });

            var inputJQueryRef = $(this.$el.querySelector("input.typeahead"));
            vm.typeaheadComponent = inputJQueryRef.typeahead({
                                                                 highlight: true,
                                                                 minLength: 3,
                                                             }, {
                                                                 name: 'models',
                                                                 templates: {
                                                                     empty: '<span style="margin-left: 1.3rem">Pas de résultats pour ce filtre</span>',
                                                                 },
                                                                 display: 'repr',
                                                                 source: modelSource,
                                                             });
            inputJQueryRef.bind('typeahead:select', function (ev, selectedModel) {
                vm.selectedModel = selectedModel;
            });
        },
        methods: {
            onConfirmSelectedElement: function onConfirmSelectedElement() {
                this.$emit('confirmedElement', this.selectedModel);
                this.selectedModel = undefined;
                this.typeaheadComponent.typeahead('val', '');
            }
        },
        template: '<div class="input-group">' +
                  ' <input class="typeahead form-control" type="text" placeholder="Taper les 3 premiers caractères...">' +
                  ' <span class="input-group-btn">' +
                  '     <button type="button" class="btn waves-effect waves-light btn-info" :disabled="!!!selectedModel"' +
                  '             @click="onConfirmSelectedElement()">' +
                  '         Ajouter' +
                  '     </button>' +
                  ' </span>' +
                  '</div>',
    });
})();
