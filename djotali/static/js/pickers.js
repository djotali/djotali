// Clock pickers

safeExec(function () {
    $('.single-input').clockpicker({
                                       placement: 'bottom',
                                       align: 'left',
                                       autoclose: true,
                                       default: 'now',

                                   });
});

safeExec(function () {
    $('.clockpicker').clockpicker({
                                      donetext: 'Done',

                                  })
                     .find('input').change(function () {
    });
});

safeExec(function () {
    $('.check-minutes').click(function (e) {
        // Have to stop propagation here
        e.stopPropagation();
        input.clockpicker('show')
             .clockpicker('toggleView', 'minutes');
    });
    if (/mobile/i.test(navigator.userAgent)) {
        $('input').prop('readOnly', true);
    }
});


// Colorpicker
safeExec(function () {
    $(".colorpicker").asColorPicker();
});
safeExec(function () {
    $(".complex-colorpicker").asColorPicker({
                                                mode: 'complex'
                                            });
});
safeExec(function () {
    $(".gradient-colorpicker").asColorPicker({
                                                 mode: 'gradient'
                                             });
});


// Date Dropper
safeExec(function () {
    jQuery('.datedropper').dateDropper();
});


// Daterange picker
safeExec(function () {
    jQuery('.date-range').datepicker({
                                         toggleActive: true
                                     });
});
safeExec(function () {
    $('.input-daterange-datepicker').daterangepicker({
                                                         buttonClasses: ['btn', 'btn-sm'],
                                                         applyClass: 'btn-danger',
                                                         cancelClass: 'btn-inverse'
                                                     });
});
safeExec(function () {
    $('.input-daterange-timepicker').daterangepicker({
                                                         timePicker: true,
                                                         format: 'MM/DD/YYYY h:mm A',
                                                         timePickerIncrement: 30,
                                                         timePicker12Hour: true,
                                                         timePickerSeconds: false,
                                                         buttonClasses: ['btn', 'btn-sm'],
                                                         applyClass: 'btn-danger',
                                                         cancelClass: 'btn-inverse'
                                                     });
});
safeExec(function () {
    $('.input-limit-datepicker').daterangepicker({
                                                     format: 'MM/DD/YYYY',
                                                     minDate: '06/01/2015',
                                                     maxDate: '06/30/2015',
                                                     buttonClasses: ['btn', 'btn-sm'],
                                                     applyClass: 'btn-danger',
                                                     cancelClass: 'btn-inverse',
                                                     dateLimit: {
                                                         days: 6
                                                     }
                                                 });
});

function safeExec(fn) {
    try {
        fn();
    } catch (ignore) {

    }
}