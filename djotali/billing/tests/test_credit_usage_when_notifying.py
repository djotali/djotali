import datetime

import pytest
from celery.exceptions import MaxRetriesExceededError
from django.utils import timezone

from djotali.billing.exceptions import NotEnoughCredit
from djotali.billing.models import Credit, CampaignAllocatedCredit
from djotali.campaigns.models import Campaign, Notification
from djotali.campaigns.tasks import notify_campaign, notify
from djotali.conftest import now_utc_plus


def _get_started_campaign():
    started_campaign = Campaign.objects.filter(start_date__lt=timezone.now()).first()
    if not started_campaign:
        started_campaign = Campaign.objects.first()
        started_campaign.start_date = now_utc_plus(datetime.timedelta(days=-2))
        started_campaign.save()
    return started_campaign


@pytest.mark.django_db()
@pytest.mark.usefixtures('seed_data')
def test_should_allocate_credit_when_campaign_is_started(mocker):
    # GIVEN
    mocker.patch('celery.canvas.group.apply_async')
    campaign = _get_started_campaign()
    campaign_id = campaign.id
    Credit.objects.update_or_create(organization=campaign.organization, defaults={'credit': 2000})

    # WHEN
    notify_campaign(campaign_id)

    # THEN
    allocated_credit = CampaignAllocatedCredit.objects.get(campaign_id=campaign_id).allocated_credit
    created_messages_count = Notification.objects.filter(campaign=campaign, status=Notification.CREATED).count()
    assert allocated_credit == created_messages_count


@pytest.mark.django_db()
@pytest.mark.usefixtures('seed_data')
@pytest.mark.usefixtures('celery_eager_mode')
@pytest.mark.usefixtures('depends_on_current_app')
@pytest.mark.usefixtures('celery_worker')
def test_should_deallocate_remaining_credit_when_campaign_ends():
    # GIVEN
    campaign = _get_started_campaign()
    campaign_id = campaign.id
    Credit.objects.update_or_create(organization=campaign.organization, defaults={'credit': 2000})

    # WHEN
    notify_campaign.delay(campaign_id).get()

    # THEN
    # Allocated credit has been soft deleted
    assert not CampaignAllocatedCredit.objects.filter(campaign__id=campaign_id).exists()
    # The allocated credit that has been soft deleted is equal to 0
    allocated_credit = CampaignAllocatedCredit.all.get(campaign__id=campaign_id).allocated_credit
    assert allocated_credit == 0
    # Sms sent has been deduced from the organization credit
    successful_notifications_count = Notification.objects.filter(campaign=campaign, status=Notification.SENT).count()
    organization_credit = Credit.objects.get(organization=campaign.organization).credit
    assert organization_credit == 2000 - successful_notifications_count


@pytest.mark.django_db()
@pytest.mark.usefixtures('seed_data')
def test_should_allocate_credit_for_notification():
    # GIVEN
    campaign = _get_started_campaign()
    campaign_id = campaign.id
    notification = Notification.objects.filter(campaign_id=campaign_id).first()
    Credit.objects.update_or_create(organization=campaign.organization, defaults={'credit': 2000})

    # WHEN
    notify(notification.id, notification.contact.phone_number, campaign.message, campaign.name, campaign_id)

    # THEN
    allocated_credit = CampaignAllocatedCredit.objects.get(campaign_id=campaign_id).allocated_credit
    assert allocated_credit == 1


@pytest.mark.django_db()
@pytest.mark.usefixtures('seed_data')
@pytest.mark.usefixtures('celery_eager_mode')
@pytest.mark.usefixtures('depends_on_current_app')
@pytest.mark.usefixtures('celery_worker')
def test_should_fail_notification_if_not_enough_credit_in_organization():
    # GIVEN
    campaign = _get_started_campaign()
    campaign_id = campaign.id
    notification = Notification.objects.filter(campaign_id=campaign_id).first()
    Credit.objects.update_or_create(organization=campaign.organization, defaults={'credit': 0})

    # WHEN
    with pytest.raises(NotEnoughCredit):
        notify.delay(notification.id, notification.contact.phone_number, campaign.message, campaign.name,
                     campaign_id).get()

    # THEN
    assert Notification.objects.get(id=notification.id).status == Notification.FAILED


@pytest.mark.django_db()
@pytest.mark.usefixtures('seed_data')
@pytest.mark.usefixtures('celery_eager_mode')
@pytest.mark.usefixtures('depends_on_current_app')
@pytest.mark.usefixtures('celery_worker')
def test_should_consume_credit_when_message_sent():
    # GIVEN
    campaign = _get_started_campaign()
    campaign_id = campaign.id
    notification = Notification.objects.filter(campaign_id=campaign_id).first()
    Credit.objects.update_or_create(organization=campaign.organization, defaults={'credit': 2000})

    # WHEN
    notify.delay(notification.id, notification.contact.phone_number, campaign.message, campaign.name, campaign_id).get()

    # THEN
    organization_credit = Credit.objects.get(organization=campaign.organization).credit
    assert organization_credit == 1999
    assert not CampaignAllocatedCredit.objects.filter(campaign_id=campaign_id).first()


@pytest.mark.django_db()
@pytest.mark.usefixtures('seed_data')
@pytest.mark.usefixtures('celery_eager_mode')
@pytest.mark.usefixtures('depends_on_current_app')
@pytest.mark.usefixtures('celery_worker')
def test_should_just_deallocate_credit_allocated_for_notification_when_failure(mocker):
    # GIVEN
    mocker.patch('djotali.core.services.ConsoleSmsSender.send', return_value=False)
    campaign = _get_started_campaign()
    campaign_id = campaign.id
    notification = Notification.objects.filter(campaign_id=campaign_id).first()
    Credit.objects.update_or_create(organization=campaign.organization, defaults={'credit': 2000})

    # WHEN
    with pytest.raises(MaxRetriesExceededError):
        notify.delay(notification.id, notification.contact.phone_number, campaign.message, campaign.name,
                     campaign_id).get()

    # THEN
    assert Credit.objects.get(organization=campaign.organization).credit == 2000
    assert CampaignAllocatedCredit.all.get(campaign=campaign).allocated_credit == 0


@pytest.mark.django_db()
@pytest.mark.usefixtures('seed_data')
@pytest.mark.usefixtures('celery_eager_mode')
@pytest.mark.usefixtures('depends_on_current_app')
@pytest.mark.usefixtures('celery_worker')
def test_should_consume_allocated_credit_when_campaign_successful():
    # GIVEN
    campaign = _get_started_campaign()
    campaign_id = campaign.id
    Credit.objects.update_or_create(organization=campaign.organization, defaults={'credit': 2000})

    # WHEN
    notify_campaign.delay(campaign_id).get()

    # THEN
    successful_message_count = Notification.objects.filter(campaign=campaign, status=Notification.SENT).count()
    assert Credit.objects.get(organization=campaign.organization).credit == 2000 - successful_message_count
    assert CampaignAllocatedCredit.all.get(campaign=campaign).allocated_credit == 0


@pytest.mark.django_db()
@pytest.mark.usefixtures('seed_data')
@pytest.mark.usefixtures('celery_eager_mode')
@pytest.mark.usefixtures('depends_on_current_app')
@pytest.mark.usefixtures('celery_worker')
def test_should_not_launch_campaign_if_not_enough_credit():
    # GIVEN
    campaign = _get_started_campaign()
    campaign_id = campaign.id
    Credit.objects.update_or_create(organization=campaign.organization, defaults={'credit': 1})

    # WHEN
    with pytest.raises(NotEnoughCredit):
        notify_campaign.delay(campaign_id).get()

    # THEN
    assert Credit.objects.get(organization=campaign.organization).credit == 1
    assert not CampaignAllocatedCredit.all.filter(campaign=campaign).exists() or CampaignAllocatedCredit.all.get(
        campaign=campaign).allocated_credit == 0


@pytest.mark.django_db()
@pytest.mark.usefixtures('seed_data')
@pytest.mark.usefixtures('celery_eager_mode')
@pytest.mark.usefixtures('depends_on_current_app')
@pytest.mark.usefixtures('celery_worker')
def test_should_reallocate_if_enough_credit_and_existing_allocation():
    # GIVEN
    campaign = _get_started_campaign()
    campaign_id = campaign.id
    Credit.objects.update_or_create(organization=campaign.organization, defaults={'credit': 2000})
    CampaignAllocatedCredit.objects.create(campaign=campaign, organization=campaign.organization, allocated_credit=0,
                                           is_removed=True)

    # WHEN
    notify_campaign.delay(campaign_id).get()

    # THEN
    successful_message_count = Notification.objects.filter(campaign=campaign, status=Notification.SENT).count()
    assert Credit.objects.get(organization=campaign.organization).credit == 2000 - successful_message_count
    assert CampaignAllocatedCredit.all.get(campaign=campaign).allocated_credit == 0
