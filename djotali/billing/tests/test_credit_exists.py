import pytest
from organizations.models import Organization

from djotali.billing.models import Credit


@pytest.mark.django_db()
def test_credit_exists_after_organization_creation():
    # GIVEN
    org_name = 'Pena-Leblanc'

    # WHEN
    Organization.objects.create(name=org_name)

    # THEN
    assert Credit.objects.get(organization__name=org_name).credit == 0
