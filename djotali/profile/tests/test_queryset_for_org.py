import pytest
from organizations import models
from organizations.models import Organization

from djotali.contacts.models import Contact


def _create_another_organization():
    models.Organization.objects.create(name='Another Organization')


def _create_contacts():
    organizations = Organization.objects.all()

    organization_0 = organizations[0]
    Contact(first_name='Ousmane', last_name='Dembele', phone_number='+2211777778', organization=organization_0).save()
    Contact(first_name='Fatou', last_name='Dembele', phone_number='+2211777777', organization=organization_0).save()

    organization_1 = organizations[1]
    Contact(first_name='Demba', last_name='Dembele', phone_number='+2211777779', organization=organization_1).save()


@pytest.mark.django_db()
@pytest.mark.usefixtures('create_test_organization')
def test_should_return_only_the_organization_contacts():
    # GIVEN
    _create_another_organization()
    _create_contacts()
    organization = Organization.objects.first()

    # WHEN
    organization_contacts_count = Contact.org_objects.get_queryset_for_organization(organization).count()

    # THEN
    assert 0 < organization_contacts_count < Contact.objects.count()
