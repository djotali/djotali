import pytest
from django.contrib.auth import models
from django.template import loader
from organizations.models import Organization

from djotali.profile.models import Sender
from djotali.profile.signals import alert_admin, create_sender

_org_name = 'Pena-Leblanc'


def _create_organization_and_user():
    organization = Organization.objects.create(name=_org_name)
    organization_owner_email = 'jason50@taylor.com'
    organization_owner = models.User.objects.create(email=organization_owner_email, username=organization_owner_email)
    organization.add_user(organization_owner)


@pytest.mark.django_db()
def test_should_send_mail_to_owner_for_another_user(mocker):
    # GIVEN
    _create_organization_and_user()
    mock_send_mail = mocker.patch('djotali.core.tasks.jobs.send_email.delay')
    request = Request({
        'organization': _org_name,
    })
    user = User('user@acme.com')

    # WHEN
    alert_admin(request, user)

    # THEN
    email_body = loader.get_template('profile/new_signup_email_body.html').render({
        'organization': 'Pena-Leblanc',
        'activate_url': '/profile/activate-users',
        'signup_email': 'user@acme.com',
        'request': request,
    })

    mock_send_mail.assert_called_once_with(
        'Nouvelle inscription sur le compte Djotali de Pena-Leblanc',
        email_body,
        'donotreply@mg.djotali.com',
        ['jason50@taylor.com'],
    )


@pytest.mark.django_db()
def test_should_not_send_mail_to_owner_for_owner(mocker):
    # GIVEN
    _create_organization_and_user()
    mock_send_mail = mocker.patch('djotali.core.tasks.jobs.send_email.delay')
    request = Request({
        'organization': _org_name,
    })
    user = User('jason50@taylor.com')

    # WHEN
    alert_admin(request, user)

    # THEN
    mock_send_mail.assert_not_called()


@pytest.mark.django_db()
def test_should_create_sender():
    # GIVEN
    _create_organization_and_user()
    sender_id = 'PenaLeblanc'
    request = Request({
        'organization': _org_name,
        'sender_id': sender_id,
    })
    user = User('user@acme.com')

    # WHEN
    create_sender(request, user)

    # THEN
    assert Sender.objects.filter(sender_id=sender_id).exists()


@pytest.mark.django_db()
def test_should_not_create_sender_if_other_characters_than_letters():
    # GIVEN
    _create_organization_and_user()
    sender_id = 'PenaLebla11'
    request = Request({
        'organization': _org_name,
        'sender_id': sender_id,
    })
    user = User('user@acme.com')

    # WHEN
    create_sender(request, user)

    # THEN
    assert not Sender.objects.filter(sender_id=sender_id).exists()


@pytest.mark.django_db()
def test_should_not_create_sender_if_more_than_11_characters():
    # GIVEN
    _create_organization_and_user()
    sender_id = 'PenaLeblancSec'
    request = Request({
        'organization': _org_name,
        'sender_id': sender_id,
    })
    user = User('user@acme.com')

    # WHEN
    create_sender(request, user)

    # THEN
    assert not Sender.objects.filter(sender_id=sender_id).exists()


class Request:
    def __init__(self, post_dict):
        self.POST = post_dict

    @staticmethod
    def build_absolute_uri(rel_uri):
        return rel_uri


class User:
    def __init__(self, email):
        self.username = email
