from datetime import date, timedelta

from django.utils.http import int_to_base36

from djotali.profile.core import InvitationTokenGenerator

_subject = InvitationTokenGenerator()


def test_should_return_true_for_a_freshly_generated_invitation_token():
    # GIVEN a freshly generated token
    email = 'botilabot@botilab.com'
    organization = 'Botilab'
    token = _subject.generate_token(email, organization)

    # WHEN checking it
    token_valid = _subject.check_token(token, email, organization)

    # THEN it is valid
    assert token_valid


def test_should_return_false_if_the_timestamp_is_in_the_past_but_valid():
    # GIVEN a valid timestamp in the past
    email = 'botilabot@botilab.com'
    organization = 'Botilab'
    timestamp = _subject._num_days(date.today() - timedelta(days=25))
    token = _subject._make_token_with_timestamp(email, organization, timestamp)

    # WHEN
    token_valid = _subject.check_token(token, email, organization)

    # THEN
    assert token_valid


def test_should_return_false_if_the_timestamp_is_expired():
    # GIVEN an expired timestamp
    email = 'botilabot@botilab.com'
    organization = 'Botilab'
    timestamp = _subject._num_days(date.today() - timedelta(days=50))
    token = _subject._make_token_with_timestamp(email, organization, timestamp)

    # WHEN
    token_valid = _subject.check_token(token, email, organization)

    # THEN
    assert not token_valid


def test_should_return_false_if_the_timestamp_is_tampered():
    # GIVEN a tampered timestamp
    email = 'botilabot@botilab.com'
    organization = 'Botilab'
    timestamp = _subject._num_days(date.today() - timedelta(days=50))
    token = _subject._make_token_with_timestamp(email, organization, timestamp)
    token = '{}-{}'.format(int_to_base36(_subject._num_days(date.today())), token.split('-')[1])

    # WHEN
    token_valid = _subject.check_token(token, email, organization)

    # THEN
    assert not token_valid


def test_should_return_false_if_the_organization_is_tampered():
    # GIVEN a tampered organization
    email = 'botilabot@botilab.com'
    organization = 'Botilab'
    token = _subject.generate_token(email, organization)

    # WHEN
    token_valid = _subject.check_token(token, email, 'Dummy')

    # THEN
    assert not token_valid


def test_should_return_false_if_the_email_is_tampered():
    # GIVEN a tampered organization
    email = 'botilabot@botilab.com'
    organization = 'Botilab'
    token = _subject.generate_token(email, organization)

    # WHEN
    token_valid = _subject.check_token(token, 'dummy@gmail.com', organization)

    # THEN
    assert not token_valid
