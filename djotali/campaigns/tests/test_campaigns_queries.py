import datetime

import arrow
import pytest
from django.utils import timezone
from organizations.models import Organization

from djotali.billing.models import CampaignAllocatedCredit
from djotali.campaigns.models import Campaign
from djotali.conftest import now_utc_plus


def _create_campaigns():
    organization = Organization.objects.first()
    Campaign.objects.create(organization=organization, name='78% Off ! Molestiae provident autem.',
                            start_date=now_utc_plus(datetime.timedelta(days=5)))
    Campaign.objects.create(organization=organization, name='Test', start_date=now_utc_plus(datetime.timedelta(days=3)))
    Campaign.objects.create(organization=organization, name='24% Off ! Saepe a dicta.',
                            start_date=now_utc_plus(datetime.timedelta(days=0)))
    Campaign.objects.create(organization=organization, name='49% Off ! Unde praesentium quasi.',
                            start_date=now_utc_plus(datetime.timedelta(days=-2)))
    Campaign.objects.create(organization=organization, name='75% Off ! Qui id.',
                            start_date=now_utc_plus(datetime.timedelta(days=-3)))
    Campaign.objects.create(organization=organization, name='60% Off ! Odio ullam ratione.',
                            start_date=now_utc_plus(datetime.timedelta(days=-5)))


def test_campaign_is_started_if_start_date_in_the_past():
    # GIVEN
    time = timezone.now() - datetime.timedelta(days=2)

    # WHEN
    opened_campaign = Campaign(start_date=time)

    # THEN
    assert opened_campaign.is_started()


@pytest.mark.django_db()
@pytest.mark.usefixtures('create_test_organization')
def test_closest_valid_campaigns_appears_first_and_then_invalid_campaigns():
    # GIVEN
    _create_campaigns()

    # WHEN
    closest_first_campaigns = Campaign.get_closest_campaigns_query_set().filter(organization_id=1).all()

    # THEN
    closest_first_campaigns_names = [campaign.name for campaign in closest_first_campaigns]
    assert closest_first_campaigns_names == [
        '24% Off ! Saepe a dicta.',
        'Test',
        '78% Off ! Molestiae provident autem.',
        '49% Off ! Unde praesentium quasi.',
        '75% Off ! Qui id.',
        '60% Off ! Odio ullam ratione.'
    ]


@pytest.mark.django_db()
@pytest.mark.usefixtures('create_test_organization')
def test_today_s_pristine_campaigns_without_any_allocated_credit():
    # GIVEN
    _create_campaigns()
    organization = Organization.objects.first()
    Campaign.objects.create(organization=organization, name='15% Off ! Saepe a dicta.',
                            start_date=now_utc_plus(datetime.timedelta(minutes=-20)))
    Campaign.objects.create(organization=organization, name='18% Off ! Saepe a dicta.',
                            start_date=now_utc_plus(datetime.timedelta(hours=3)))
    Campaign.objects.create(organization=organization, name='19% Off ! Saepe a dicta.',
                            start_date=now_utc_plus(datetime.timedelta(minutes=1)))

    # WHEN
    today_s_pristine_campaigns_names = [
        campaign.name
        for campaign in Campaign.get_close_pristine_campaigns().all()
    ]

    # THEN
    assert today_s_pristine_campaigns_names == [
        '24% Off ! Saepe a dicta.',
        '15% Off ! Saepe a dicta.',
        '19% Off ! Saepe a dicta.',
    ]


@pytest.mark.django_db()
@pytest.mark.usefixtures('create_test_organization')
def test_today_s_pristine_campaigns_with_deleted_allocated_credit():
    # GIVEN
    _create_campaigns()
    organization = Organization.objects.first()
    Campaign.objects.create(organization=organization, name='15% Off ! Saepe a dicta.',
                            start_date=now_utc_plus(datetime.timedelta(minutes=-1)))
    Campaign.objects.create(organization=organization, name='18% Off ! Saepe a dicta.',
                            start_date=now_utc_plus(datetime.timedelta(hours=4)))
    launched_campaign = Campaign.objects.create(organization=organization, name='19% Off ! Saepe a dicta.',
                                                start_date=now_utc_plus(datetime.timedelta(minutes=-3)))
    CampaignAllocatedCredit.objects.create(campaign=launched_campaign, organization=organization, allocated_credit=0,
                                           is_removed=True)

    # WHEN
    today_s_pristine_campaigns_names = [
        campaign.name
        for campaign in Campaign.get_close_pristine_campaigns().all()
    ]

    # THEN
    assert today_s_pristine_campaigns_names == [
        '24% Off ! Saepe a dicta.',
        '15% Off ! Saepe a dicta.',
    ]


def test_campaigm_with_future_date_is_not_started():
    time = arrow.utcnow().shift(days=1)
    future_campaign = Campaign(start_date=time)
    assert not future_campaign.is_started()
