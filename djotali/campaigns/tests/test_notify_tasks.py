import datetime

import pytest
from django.utils import timezone
from organizations.models import Organization

from djotali.billing.models import Credit
from djotali.campaigns.models import Campaign, Notification
from djotali.campaigns.tasks import notify, set_up_campaigns_to_be_launched
from djotali.conftest import now_utc_plus
from djotali.contacts.templatetags.contacts_extras import format_number


def _create_campaigns():
    organization = Organization.objects.first()
    Campaign.objects.create(id=1, organization=organization, name='78% Off ! Molestiae provident autem.',
                            start_date=now_utc_plus(datetime.timedelta(days=5)))
    Campaign.objects.create(id=2, organization=organization, name='Test',
                            start_date=now_utc_plus(datetime.timedelta(days=3)))
    Campaign.objects.create(id=3, organization=organization, name='24% Off ! Saepe a dicta.',
                            start_date=now_utc_plus(datetime.timedelta(days=0)))
    Campaign.objects.create(id=4, organization=organization, name='49% Off ! Unde praesentium quasi.',
                            start_date=now_utc_plus(datetime.timedelta(days=-2)))
    Campaign.objects.create(id=5, organization=organization, name='75% Off ! Qui id.',
                            start_date=now_utc_plus(datetime.timedelta(days=-3)))
    Campaign.objects.create(id=6, organization=organization, name='60% Off ! Odio ullam ratione.',
                            start_date=now_utc_plus(datetime.timedelta(days=-5)))
    Campaign.objects.create(id=7, organization=organization, name='15% Off ! Saepe a dicta.',
                            start_date=now_utc_plus(datetime.timedelta(minutes=-10)))
    Campaign.objects.create(id=8, organization=organization, name='18% Off ! Saepe a dicta.',
                            start_date=now_utc_plus(datetime.timedelta(hours=4)))
    Campaign.objects.create(id=9, organization=organization, name='19% Off ! Saepe a dicta.',
                            start_date=now_utc_plus(datetime.timedelta(minutes=2)))


@pytest.mark.django_db()
@pytest.mark.usefixtures('seed_data')
def test_should_try_to_send_sms_to_contact_with_formatted_number(mocker):
    # GIVEN
    console_sender_send = mocker.patch('djotali.core.services.ConsoleSmsSender.send')
    campaign = Campaign.objects.filter(start_date__lt=timezone.now()).first()
    Credit.objects.update_or_create(organization=campaign.organization, defaults={'credit': 2000})
    notification = Notification.objects.filter(campaign=campaign).first()

    # WHEN
    notify(notification.id, notification.contact.phone_number, campaign.message, campaign.name, campaign.id)

    # THEN
    assert console_sender_send.call_count == 1
    console_sender_send.assert_called_with(campaign.organization.slug[:11],
                                           format_number(notification.contact.phone_number), campaign.message)


@pytest.mark.django_db()
@pytest.mark.usefixtures('create_test_organization')
def test_should_launch_the_pristine_campaigns_of_the_day_from_midnight_to_next_hour(mocker):
    # GIVEN
    mock_celery_running = mocker.patch('djotali.core.utils.is_celery_running')
    mock_celery_running.return_value = True
    mock_notify_campaign = mocker.patch('djotali.campaigns.tasks.notify_campaign.apply_async')
    _create_campaigns()
    expected_countdowns = {
        3: 0,
        7: 0,
        9: datetime.timedelta(minutes=3).total_seconds(),
    }

    # WHEN
    set_up_campaigns_to_be_launched()

    # THEN
    assert mock_notify_campaign.call_count == 3
    call_args_list = mock_notify_campaign.call_args_list
    for call_args in call_args_list:
        args, kwargs = call_args
        # apply_async takes a tuple
        campaign_id = args[0][0]
        assert kwargs['countdown'] <= expected_countdowns[campaign_id]


@pytest.mark.django_db()
@pytest.mark.usefixtures('seed_data')
def test_should_mark_the_notification_as_in_progress(mocker):
    # GIVEN
    mocker.patch('djotali.core.services.ConsoleSmsSender.send')
    tag_notif_as_in_progress = mocker.patch('djotali.campaigns.models.Notification.tag_as_in_progress')
    campaign = Campaign.objects.filter(start_date__lt=timezone.now()).first()
    Credit.objects.update_or_create(organization=campaign.organization, defaults={'credit': 2000})
    notification = Notification.objects.filter(campaign=campaign).first()

    # WHEN
    notify(notification.id, notification.contact.phone_number, campaign.message, campaign.name, campaign.id)

    # THEN
    assert tag_notif_as_in_progress.call_count == 1
