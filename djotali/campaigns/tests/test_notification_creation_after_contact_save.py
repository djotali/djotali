import pytest
from django.utils import timezone
from organizations.models import Organization

from djotali.campaigns.models import Notification, Campaign
from djotali.contacts.models import Contact, ContactsGroup


def _create_campaign_linked_to_all_contacts_group():
    organization = Organization.objects.first()
    Campaign.objects.create(id=42, name='Test campaign', organization=organization, start_date=timezone.now())


def _create_barca_contacts_group():
    organization = Organization.objects.first()
    dembouz = Contact.objects.create(first_name='Ousmane', last_name='Dembele', phone_number='+2211777778',
                                     organization=organization)
    thiago = Contact.objects.create(first_name='Thiago', last_name='Alcantara', phone_number='+2211777779',
                                    organization=organization)
    barca_group = ContactsGroup.objects.create(organization=organization, name='Barca')
    barca_group.contacts.add(dembouz)
    barca_group.contacts.add(thiago)
    return barca_group


def _create_madrid_contacts_group():
    organization = Organization.objects.first()
    zizou = Contact.objects.create(first_name='Zinedine', last_name='Zidane', phone_number='+2211777775',
                                   organization=organization)
    modric = Contact.objects.create(first_name='Luca', last_name='Modric', phone_number='+2211777776',
                                    organization=organization)
    asensio = Contact.objects.create(first_name='Marco', last_name='Asensio', phone_number='+2211777780',
                                     organization=organization)
    madrid_group = ContactsGroup.objects.create(organization=organization, name='Madrid')
    madrid_group.contacts.add(zizou)
    madrid_group.contacts.add(modric)
    madrid_group.contacts.add(asensio)
    return madrid_group


@pytest.mark.django_db()
@pytest.mark.usefixtures('create_test_organization')
def test_should_create_notification_for_campaign_linked_to_all_contacts_group():
    # GIVEN
    _create_campaign_linked_to_all_contacts_group()
    organization = Organization.objects.first()
    dembouz = Contact(first_name='Ousmane', last_name='Dembele', phone_number='+2211777778', organization=organization)

    # WHEN
    dembouz.save()

    # THEN
    assert Notification.objects.filter(contact=dembouz, campaign=42).exists()


@pytest.mark.django_db()
@pytest.mark.usefixtures('create_test_organization')
def test_should_create_notifications_for_created_campaign():
    # GIVEN
    organization = Organization.objects.first()
    barca_group = _create_barca_contacts_group()

    # WHEN
    Campaign.objects.create(id=42, name='Test campaign', organization=organization, start_date=timezone.now(),
                            contacts_group=barca_group)

    # THEN
    _assert_campaign_notifications_contacts_equals(42, 'Thiago Alcantara', 'Ousmane Dembele')


@pytest.mark.django_db()
@pytest.mark.usefixtures('create_test_organization')
def test_should_create_notifications_for_updated_campaign():
    # GIVEN
    organization = Organization.objects.first()
    barca_group = _create_barca_contacts_group()
    madrid_group = _create_madrid_contacts_group()
    campaign = Campaign.objects.create(id=42, name='Test campaign', organization=organization,
                                       start_date=timezone.now(), contacts_group=barca_group)

    # WHEN
    campaign.contacts_group = madrid_group
    campaign.save()

    # THEN
    _assert_campaign_notifications_contacts_equals(42, 'Zinedine Zidane', 'Luca Modric', 'Marco Asensio')


@pytest.mark.django_db()
@pytest.mark.usefixtures('create_test_organization')
def test_should_create_notifications_for_updated_campaign_and_same_contact_in_two_groups():
    # GIVEN
    organization = Organization.objects.first()
    barca_group = _create_barca_contacts_group()
    madrid_group = _create_madrid_contacts_group()
    dembouz = Contact.objects.get(first_name='Ousmane')
    madrid_group.contacts.add(dembouz)
    campaign = Campaign.objects.create(id=42, name='Test campaign', organization=organization,
                                       start_date=timezone.now(), contacts_group=barca_group)

    # WHEN
    campaign.contacts_group = madrid_group
    campaign.save()

    # THEN
    _assert_campaign_notifications_contacts_equals(42, 'Zinedine Zidane', 'Luca Modric', 'Marco Asensio',
                                                   'Ousmane Dembele')


def _assert_campaign_notifications_contacts_equals(campaign_id, *args):
    notifications_contacts = [
        str(notification.contact)
        for notification in Notification.objects.filter(campaign=campaign_id).all()
    ]
    assert sorted(notifications_contacts) == sorted(args)
