import logging

from django.apps import AppConfig
from django.conf import settings
from django.utils.functional import cached_property

from djotali.core.services import ConsoleSmsSender, MessageBirdSmsSender

_logger = logging.getLogger(__name__)


class CoreConfig(AppConfig):
    name = 'djotali.core'
    verbose_name = 'core'

    def ready(self):
        from django.contrib.auth.admin import UserAdmin
        UserAdmin.list_display = ('id',) + UserAdmin.list_display

    @cached_property
    def sms_sender(self):
        if settings.MESSAGEBIRD_TOKEN:
            return MessageBirdSmsSender(settings.MESSAGEBIRD_TOKEN)
        return ConsoleSmsSender(settings.CONSOLE_SMS_MAX_TIMEOUT, settings.CONSOLE_SMS_MIN_TIMEOUT)
