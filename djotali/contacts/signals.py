# coding: utf-8
from djotali.contacts.templatetags.contacts_extras import format_number


def format_number_for_db(phone_number):
    return format_number(phone_number).replace(' ', '')


def before_contact_saved(sender, instance, **kwargs):
    instance.phone_number = format_number_for_db(instance.phone_number)
