# coding: utf-8

import unicodecsv as csv
from io import BytesIO
from django.db import transaction
from faker.factory import Factory

from djotali.contacts.models import Contact
from djotali.contacts.signals import format_number_for_db
from djotali.contacts.templatetags.contacts_extras import format_number
from djotali.core.seed import SenegalesePhoneNumberProvider


class CsvUtil:
    def __init__(self, delimiter=";"):
        self.delimiter = delimiter

    def create_csv_model(self, headers=None):
        if headers is None:
            headers = ['Nom', 'Prenom', 'Telephone']

        csv_model_io = BytesIO()
        writer = csv.writer(csv_model_io, delimiter=self.delimiter, quoting=csv.QUOTE_MINIMAL)
        writer.writerow(headers)
        faker = Factory.create('fr_FR')
        faker.add_provider(SenegalesePhoneNumberProvider)
        phone_number = format_number(faker.senegal_phone_number())
        writer.writerow([
            faker.first_name(),
            faker.last_name(),
            phone_number,
        ])
        # After writing, we get back to the beginning of the file
        # https://stackoverflow.com/questions/26879981/writing-then-reading-in-memory-bytes-bytesio-gives-a-blank-result
        csv_model_io.seek(0)
        return csv_model_io

    def _format(self, row):
        return self.delimiter.join(row)

    def import_csv(self, csv_file, organization):
        # Process into Celery Task ?
        reader = csv.reader(csv_file, delimiter=self.delimiter)
        next(reader)
        existing_numbers = [contact.phone_number for contact in Contact.objects.filter(organization=organization).all()]
        contacts_to_create = []
        contacts_to_update = []
        for row in reader:
            phone_number = format_number_for_db(row[2])
            contact = Contact(last_name=row[0], first_name=row[1], phone_number=phone_number, organization=organization)
            if phone_number in existing_numbers:
                contacts_to_update.append(contact)
            else:
                contacts_to_create.append(contact)
        if len(contacts_to_create) > 0:
            Contact.objects.bulk_create(contacts_to_create, batch_size=20)
        if len(contacts_to_update) > 0:
            with transaction.atomic():
                for contact in contacts_to_update:
                    Contact.objects.filter(organization=organization, phone_number=contact.phone_number) \
                        .update(first_name=contact.first_name, last_name=contact.last_name)

        if len(contacts_to_create) + len(contacts_to_update) < 1:
            raise ValueError('No Data into CSV File')
