import pytest
from django.core.exceptions import ValidationError
from organizations.models import Organization

from djotali.contacts.forms import ContactForm
from djotali.contacts.models import Contact


@pytest.mark.django_db()
@pytest.mark.usefixtures('create_test_organization')
def test_should_delete_spaces_in_phone_number_before_save():
    # GIVEN
    organization = Organization.objects.first()
    moussa_dembele = Contact(first_name='Moussa', last_name='Dembele', phone_number='+221 78 177 77 78', organization=organization)

    # WHEN
    moussa_dembele.save()

    # THEN
    assert moussa_dembele.phone_number == '+221781777778'


def test_form_should_reject_phone_number_if_not_correct():
    # GIVEN
    form = ContactForm()
    form.cleaned_data = {'phone_number': '+2217817777', }

    # THEN
    with pytest.raises(ValidationError):
        # WHEN
        form.clean_phone_number()


def test_form_should_return_phone_number_if_correct():
    # GIVEN
    form = ContactForm()
    form.cleaned_data = {'phone_number': '+221 78 177 77 78', }

    # WHEN
    phone_number = form.clean_phone_number()

    # THEN
    assert phone_number == '+221 78 177 77 78'
