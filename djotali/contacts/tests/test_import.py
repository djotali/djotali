# coding: utf-
import rest_framework.status as status
from django.conf import settings
from django.contrib.auth.models import User
from django.core.files.uploadedfile import SimpleUploadedFile
from django.test import TestCase
from django.urls.base import reverse
from organizations.models import Organization

from djotali.contacts.models import Contact

_santa_email = 'santa.claus@gmail.com'


class ImportContactsTest(TestCase):
    def setUp(self):
        if not User.objects.filter(username=_santa_email).exists():
            santa = User.objects.create_user(_santa_email, _santa_email, '123456')
            organization = Organization.objects.create(name='Christmas')
            organization.add_user(santa)
            self.organization = organization
        self.file_content = "\n".join([
            "Nom;Prenom;Telephone",
            "Doe;John;771234567",
            "Doe;Jane;783452312",
        ])
        self.client.login(username=_santa_email, password='123456')

    def test_import_valid_csv_file(self):
        # Given a valid csv file
        csv_file = SimpleUploadedFile(
            settings.MEDIA_URL + '/test_import.csv',
            bytearray(self.file_content.strip(), 'utf-8'),
            content_type='text/csv')

        # When i submit valid csv file
        response = self.client.post(reverse('contacts:import'), {'file': csv_file})

        # Contacts should be created
        self.assertRedirects(response, reverse('dashboard:index'), target_status_code=200)
        print(Contact.objects.all())
        self.assertTrue(
            Contact.objects.filter(first_name="John", last_name="Doe", phone_number="+221771234567").exists()
        )
        self.assertTrue(
            Contact.objects.filter(first_name="Jane", last_name="Doe", phone_number="+221783452312").exists()
        )

    def test_import_valid_csv_file_should_update_existing_contact(self):
        # Given a valid csv file and an existing contact
        Contact.objects.create(first_name="Sadio", last_name="Mane", phone_number="771234567", organization=self.organization)
        csv_file = SimpleUploadedFile(
            settings.MEDIA_URL + '/test_import.csv',
            bytearray(self.file_content.strip(), 'utf-8'),
            content_type='text/csv')

        # When i submit valid csv file
        response = self.client.post(reverse('contacts:import'), {'file': csv_file})

        # Contacts should be created
        self.assertRedirects(response, reverse('dashboard:index'), target_status_code=200)
        self.assertTrue(
            Contact.objects.filter(first_name="John", last_name="Doe", phone_number="+221771234567").exists()
        )
        self.assertTrue(
            Contact.objects.filter(first_name="Jane", last_name="Doe", phone_number="+221783452312").exists()
        )

    def test_get_csv_model(self):
        # When i hit csv model url
        response = self.client.get(reverse('contacts:csv_model'))

        # Csv model should be downloaded
        self.assertTrue(status.is_success(response.status_code))
        self.assertEquals(
            response.get('Content-Disposition'),
            "attachment; filename=model.csv"
        )
        # Csv Header should be correct
        self.assertEqual(
            response.content.decode().split()[0],
            "Nom;Prenom;Telephone"
        )

    def test_import_invalid_model(self):
        # Given an invalid csv file
        cvs_file = None

        # When i submit invalid csv file
        response = self.client.post(reverse('contacts:import'), {'file': cvs_file})

        # Contacts should not be created
        self.assertEqual(response.status_code, status.HTTP_422_UNPROCESSABLE_ENTITY)
